from numpy import ndarray
from py_pol.utils import degrees
from py_pol.jones_vector import Jones_vector
from py_pol.jones_matrix import Jones_matrix
import numpy as np
from simulationLogic import jones_matrix as jm
import copy


class OpticClass:
    def __init__(self):
        self.finalWaveAfterOpticsElement = None
        self.originalWave = None
        self.E0 = None
        self.opticElementsArray = []
        self.backwardOpticElementsArray = []
        self.num_of_steps = 1601
        self.waveLengthWindow = np.linspace(610, 950, 1601)
        self.analyzerIsNotSet = True

    def setWaveWindow(self, opticWindow):
        self.waveLengthWindow = np.linspace(opticWindow['from'], opticWindow['to'], 1601)
        return True

    def clearOpticElementsList(self):
        self.opticElementsArray = []
        self.backwardOpticElementsArray = []
        return True

    def setStraighScheme(self, arrayData):
        self.analyzerIsNotSet = True
        for opticLine in arrayData:
            # currentLineId = arrayData[i].id
            if self.analyzerIsNotSet:
                for opticElement in opticLine['scheme']:
                    if opticElement['type'] == 'circular':
                        continue
                    elif opticElement['type'] == 'analyzer':
                        self.analyzerIsNotSet = False
                        break
                    elif opticElement['type'] == 'mirror':
                        for j in range(len(opticLine['scheme'])-2, -1, -1):
                            if opticElement['type'] == 'circular':
                                continue
                            else:
                                backwardElem = copy.deepcopy(opticLine['scheme'][j])
                                backwardElem['isBackward'] = True
                                self.setJonesMatrix(backwardElem)
                    else:
                        opticElement['isBackward'] = False
                        self.setJonesMatrix(opticElement)

    def setJonesMatrix(self, elementObj):
        if elementObj['type'] == 'polarize':
            self.setPolarizer(elementObj['polarizeAngel'])
        if elementObj['type'] == 'isolator1' and elementObj['isBackward'] == False:
            self.faradayRotationElement(elementObj['lengthOfCrystal']/1000, elementObj['inductionOfExternalField'])
        elif elementObj['type'] == 'isolator1' and elementObj['isBackward']:
            self.backwardFaradayElement(elementObj['lengthOfCrystal']/1000, elementObj['inductionOfExternalField'])
        # elif elementObj['type'] == 'filter':
        #     self.setOpticFilter(elementObj['value'])

    def getPolarizationDependsOfType(self, wave_type, direction, angel=0, intensity=1, aLong=None, bLong=None):
        if wave_type == 'circular':
            return self.getCircularPolarization(direction)
        if wave_type == 'linear':
            return self.getLinearPolarization(angel, intensity)
        if wave_type == 'ellipse':
            return self.getEllipsePolarization(direction, angel, intensity, aLong, bLong)

    def getLinearPolarization(self, angel, intensity=1):
        self.E0 = Jones_vector('original wave')
        radAngel = (np.pi*angel)/180
        self.originalWave = self.E0.linear_light(azimuth=radAngel, intensity=intensity)
        parameters = self.originalWave.parameters
        originalWaveParam = {
            'a': parameters.ellipse_axes()[0],
            'b': parameters.ellipse_axes()[1],
            'azimuth': parameters.azimuth()*1,
            'intensity': parameters.intensity(),
            'E0x': parameters.amplitudes()[0],
            'E0y': parameters.amplitudes()[1],
            'alpha': parameters.alpha()*1,
            'delay': parameters.delay(),
            'degree_circular_polarization': parameters.degree_circular_polarization(),
            'degree_linear_polarization': parameters.degree_linear_polarization()
        }
        return originalWaveParam

    def getEllipsePolarization(self, direction, angel, intensity=1, aLong=0, bLong=0):
        # d','r' - right, dextro
        # 'l', 'i' - left, levo
        # leftHanded и rightHanded поменяны местами...
        self.E0 = Jones_vector('original wave')
        self.originalWave = self.E0.elliptical_light(a=aLong, b=bLong,
                                                     kind=direction,
                                                     azimuth=angel*np.pi/180)
        return {'angel': angel*np.pi/180}

    def getCircularPolarization(self, direction, intensity=1):
        # d','r' - right, dextro
        # 'l', 'i' - left, levo
        # from_matrix(jm.getCircularWave(direction))
        # leftHanded и rightHanded поменяны местами...
        self.E0 = Jones_vector('original wave')
        self.originalWave = self.E0.circular_light(kind=direction, intensity=intensity)
        return 'True'

    def setPolarizer(self, angel):
        M0 = Jones_matrix('Polarizer')
        M0.diattenuator_perfect(azimuth=angel*np.pi/180)
        self.opticElementsArray.append(M0)
        self.backwardOpticElementsArray.insert(0, M0)

    def getResult(self):
        waveIntermediate = copy.deepcopy(self.originalWave)
        backwardWave = self.originalWave.reciprocal(keep=True)

        if len(self.opticElementsArray) >= 1:
            for opticElem in self.opticElementsArray:
                waveIntermediate = opticElem * waveIntermediate

        if len(self.opticElementsArray) >= 1:
            for opticElem in self.backwardOpticElementsArray:
                backwardWave = opticElem * backwardWave

        self.finalWaveAfterOpticsElement = waveIntermediate
        parameters = waveIntermediate.parameters
        resultForSend = {}
        if(parameters.intensity().size > 1):
            resultForSend = {
                'intensity': list(parameters.intensity()),
                'opticWindowFrom': self.waveLengthWindow[0],
                'opticWindowTo': self.waveLengthWindow[-1],
                'intensityBackward': list(backwardWave.parameters.intensity())
            }
        else:
            resultForSend = {
                'a': parameters.ellipse_axes()[0],
                'b': parameters.ellipse_axes()[1],
                'azimuth': parameters.azimuth() * 1,
                'alpha': parameters.alpha(),
                'intensity': parameters.intensity(),
                'E0x': parameters.amplitudes()[0],
                'E0y': parameters.amplitudes()[1],
                'degree_circular_polarization': parameters.degree_circular_polarization(),
                'degree_linear_polarization': parameters.degree_linear_polarization(),
                'my_angel': (parameters.alpha() * 180 / np.pi) * np.cos(parameters.delay())
            }

        return resultForSend

    def faradayRotationElement(self, L, B):
        theta = FaradayRotationElements.angelOfFaradayRotator(self.waveLengthWindow, L, B)
        R_theta = Jones_matrix("rotationAtWaveLengthAngle")
        R_theta.from_matrix(FaradayRotationElements.rotateMatrix(theta))
        M0 = Jones_matrix('PolarizerBefore')
        M0.diattenuator_perfect(azimuth=0)
        M1 = Jones_matrix('PolarizerAfter')
        M1.diattenuator_perfect(azimuth=45 * degrees)

        self.opticElementsArray.append(M1 * R_theta * M0)
        self.backwardOpticElementsArray.insert(0, M1.reciprocal(keep=True) * R_theta * M0.reciprocal(keep=True))

    def backwardFaradayElement(self, L, B):
        theta = FaradayRotationElements.angelOfFaradayRotator(self.waveLengthWindow, L, B)
        R_theta = Jones_matrix("rotationAtWaveLengthAngle")
        R_theta.from_matrix(FaradayRotationElements.rotateMatrix(theta))
        M0 = Jones_matrix('PolarizerBefore')
        M0.diattenuator_perfect(azimuth=0)
        M1 = Jones_matrix('PolarizerAfter')
        M1.diattenuator_perfect(azimuth=45 * degrees)

        self.opticElementsArray.append(M1.reciprocal(keep=True) * R_theta * M0.reciprocal(keep=True))
        self.backwardOpticElementsArray.insert(0, M1 * R_theta * M0)

class FaradayRotationElements:
    def rotateMatrix(azimuth):  # оператор поворота на azimuth
        return np.array([[np.cos(azimuth), np.sin(azimuth)],
                         [-np.sin(azimuth), np.cos(azimuth)]])

    def VerdetsDispersion(waveLenght):  # учет дисперсии
        # lambda0 = 436
        # K = - 2.31 * 10 ** 7
        # return K / (waveLenght ** 2 - lambda0 ** 2)
        lambda0 = 436
        # K = -6.195 * 10 ** 7
        K = -6.865 * 10 ** 8
        return K / (waveLenght ** 2 - lambda0 ** 2)

    def angelOfFaradayRotator(waveLenght, L=0.01912, B=1):  # угол дисперсии #L= 0,01912 для поворота на pi/2
        dispersion = FaradayRotationElements.VerdetsDispersion(waveLenght)
        return dispersion * B * L
