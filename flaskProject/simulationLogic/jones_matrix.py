import numpy as np


def getCircularWave(direction):
    circular_matrix = None
    if direction == 'l':
        circular_matrix = np.array([1 / np.sqrt(2), -1j / np.sqrt(2)])
    elif direction == 'r':
        circular_matrix = np.array([1 / np.sqrt(2), 1j / np.sqrt(2)])
    return circular_matrix


def getLinearMatrixPol(theta):
    M = np.array([[np.cos(theta) ** 2, np.cos(theta) * np.sin(theta)],
                  [np.cos(theta) * np.sin(theta), np.cos(theta) ** 2]])
    return M
