from flask import Flask, url_for, render_template, jsonify, request
from flask_cors import CORS
from markupsafe import escape
import simulationLogic
import redis
from rq import Worker, Queue, Connection

app = Flask(__name__)
CORS(app)
logic = simulationLogic.OpticClass()

@app.route('/', methods=['GET', 'POST'])
def landing():
    msg = f'Get Запрос {logic.index(2)}'
    if request.method == 'POST':
        if request.form['myInput']:
            return render_template('FirstHTML.html', var='Удачный post Запрос ' + request.form['myInput'])
        else:
            app.logger.debug('A value for debugging', 123)
            msg = 'Invalid username/password'
        # the code below is executed if the request method
        # was GET or the credentials were invalid
    return render_template('FirstHTML.html', var=msg)

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return f'Post {escape(post_id)}'

@app.route('/simulation/', methods=['GET', 'POST'])
def simulation_page():
    # show the subpath after /path/
    return 'simulationPage'

@app.route('/setIsolator', methods=['GET', 'POST'])
def set_isolator():
    return jsonify(abc=request.json['inductionOfExternalField'],
                   cdq=request.json['isolatorType'],
                   nsa=request.json['lengthOfCrystal'])

@app.route('/setExistWave', methods=['GET', 'POST'])
def set_exist_wave():
    a = 0
    # if request.method == 'POST':
    wave_type = request.json['typeOfPol']
    direction = None
    angel = 0
    intensity = 1
    aLong = None
    bLong = None
    if 'angel' in request.json:
        angel = request.json['angel']
    if 'intensity' in request.json:
        intensity = request.json['intensity']
    if wave_type == 'ellipse':
        aLong = request.json['aLong']
        bLong = request.json['bLong']
    if wave_type != 'linear':
        direction = request.json['which_direction']

    a = logic.getPolarizationDependsOfType(wave_type, direction, angel, intensity, aLong, bLong)
    return a

@app.route('/getPolarization', methods=['GET', 'POST'])
def getPolarization():
    logic.setWaveWindow(request.json['waveWindow'])
    logic.clearOpticElementsList()
    logic.setStraighScheme(request.json['arrayData'])
    # for obj in req:
    #     if obj['type'] == 'polarize':
    #         logic.setPolarizer(obj['polarizeAngel'])
    #     if obj['type'] == 'isolator1':
    #         logic.faradayRotationElement(obj['lengthOfCrystal']/1000, obj['inductionOfExternalField'])
        # app.logger.debug('A value for debugging123', )
    return logic.getResult()

@app.route('/login')
def login_get():
    return url_for('static', filename='helloWorld.js')

@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404

with app.test_request_context():
    print(url_for('login_get'))
    print(url_for('login_get', next='/'))
    print(url_for('show_post', post_id='10'))

if __name__ == '__main__':
    app.run()
