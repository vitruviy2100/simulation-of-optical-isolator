import { createWebHistory, createRouter } from "vue-router";
import Isolators from "../pages/Isolators.vue";
import MainScheme from '../pages/MainScheme.vue';

const routes = [
  {
    path: "/",
    name: "Home",
    component: Isolators,
  },
  {
    path: "/optical_design",
    name: "Optical design",
    component: MainScheme,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;