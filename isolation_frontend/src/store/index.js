import { createStore } from 'vuex'


const store = createStore({
  state: {
    originalWave: {},
    scheme: [
      {
        /* mainLine */
        id: 0,
        lineDirection: 'right',
        scheme: [] 
      }
    ],
    outputwave: {},
    waveWindow: {
      from: 610,
      to: 950
    }
  },
  getters: {},
  actions: {},
  mutations: {
    SET_ORIGINAL_WAVE(state, waveParameters) {
      state.originalWave = Object.assign({}, waveParameters)
    },
    ADD_OPTIC_ELEM(state, {opticElem, lineIndex}) {
      state.scheme[lineIndex].scheme.push(opticElem)
    },
    DELETE_OPTIC_ELEM(state, {lineIndex, index}) {
      if (state.scheme[lineIndex].scheme[index].type === 'circular') {
        state.scheme[lineIndex].scheme[index].childrenLines.forEach(deletedLineId => {
          const findedIndex = state.scheme.findIndex((element) => (element.id === deletedLineId)) 
          state.scheme.splice(findedIndex, 1)
        })
      }
      state.scheme[lineIndex].scheme.splice(index, 1)
    },
    SAVE_OPTION_OF_OPTIC_ELEM(state, {lineIndex, index, prop, val}) {
      state.scheme[lineIndex].scheme[index][prop] = val
    },
    SAVE_POL_RESPONSE(state, response) {
      state.outputwave = Object.assign({}, response)
      if (response.degree_linear_polarization === 1 || response.degree_linear_polarization === -1) {
        state.outputwave.type = 'linear'
      } else if (response.degree_circular_polarization === 1 || response.degree_circular_polarization === -1) {
        state.outputwave.type = 'circular'
      } else {
        state.outputwave.type = 'ellipse'
      }
    },
    SAVE_WAVE_WINDOW(state, {prop, value}) {
      state.waveWindow[prop] = value
    }
  }
})

export default store